import fs from 'node:fs/promises';
import { createWriteStream } from 'node:fs';
import https from 'node:https';
import puppeteer from "puppeteer";
import __dirname from 'path';

const productLinks = [
    'https://www.jdsports.co.uk/product/blue-adidas-originals-handball-spezial/1292478/?fbclid=IwAR3qNUCfqNGr4phrf8v52HSKBoQRg5127yoWqDRCZz03ItzTrKxKIXeZHIQ',
    'https://www.jdsports.co.uk/product/blue-adidas-originals-gradient-t-shirt/19601390/',
];

(async ()=> {
    const browser = await puppeteer.launch({headless: true});
    const page = await browser.newPage();
    let products = [];
    for(let i=0;i<productLinks.length;i++) {
        await page.goto(productLinks[i]);
  
        if(i==0) {
            // click on accept all cookies button
            const acceptAllCookies = '.accept-all-cookies';
            await page.waitForSelector('.accept-all-cookies');
            await page.click(acceptAllCookies);

            // if exist #myModal div , click on close button
            const myModal = '#myModal';
            const close = '.close';
            await page.waitForSelector(myModal);
            await page.waitForSelector(close);
            await page.click(close);
        }

        // get product information
        const productItemTitle = '#productItemTitle';
        const div = await page.waitForSelector(productItemTitle);
        
        // get title
        const h1 = await div.$('h1');
        const title = await h1.evaluate(el => el.textContent);

        // get price
        const priceDiv = await div.$('.itemPrices');
        const span = await priceDiv.$('.pri');
        let spanNow = await span.$('.now');
        let price = 0;
        if(spanNow === null) {
            price = await span.evaluate(el=>el.textContent);
        }
        else {
            spanNow = await spanNow.$('span');
            price = await spanNow.evaluate(el=>el.textContent);
        }
        
        // get images
        const ul = await page.waitForSelector('#alt-gallery');
        const imgs = await ul.$$('img');
        const imgSource = [];
        for(let i=0;i<imgs.length;i++) {
            imgSource.push(await imgs[i].evaluate(e=>e.getAttribute('src')));
        }

        let product = {
            title : title,
            price : price,
            img : imgSource
        }

        products.push(product);
    }
    console.log(products);
    await saveProduct(products);
    await browser.close();
})();

async function saveProduct(products) {
    const productsFolderName = 'products';
    let productFolderName;
    await fs.rm(productsFolderName, { recursive : true, force : true});
    await fs.mkdir(productsFolderName, {'recursive' : false});
    for(let i=0;i<products.length;i++) {
        productFolderName = productsFolderName + '/' + products[i].title;
        await fs.mkdir(productFolderName);

        for(let j=0;j<products[i].img.length;j++) {
            https.get(products[i].img[j], function(response){     
                const productFolderName = productsFolderName + '/' + products[i].title;       
                const contentType = response.headers['content-type'].split('/');
                const fileExtension = contentType[1];
                const fileName = getFileName(products[i].img[j]) + '.' + fileExtension;
                const file = createWriteStream(productFolderName + '/' + fileName);
                response.pipe(file);
                file.on('finish',()=>{
                    file.close();
                });
            });
        }
    }
}

/**
 * Get file name from url without extension
 * @param string imageUrl 
 * @returns string
 */
function getFileName(imageUrl) {
    let imgUrlArray = imageUrl.split('/');
    imgUrlArray = imgUrlArray[5].split('?');
    return imgUrlArray[0];
}
